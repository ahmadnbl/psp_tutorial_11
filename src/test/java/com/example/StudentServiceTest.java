package com.example;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.StudentModel;
import com.example.service.StudentService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudentServiceTest {
	
	@Autowired
	StudentService service;
	
	@Test
	public void testSelectAllStudents(){
		List<StudentModel> students = service.selectAllStudents();
		Assert.assertNotNull("Gagal = students menghasilkan null ", students);
		Assert.assertEquals("Gagal - size students tidak sesuai", 5, students.size());
	}
	
	@Test
	public void testSelectAllStudent(){
		StudentModel studentResult = service.selectStudent("1606954691");
		Assert.assertNotNull("Gagal = studentResult menghasilkan null ", studentResult);
		Assert.assertEquals("Gagal = nama mahasiswa tidak sesuai", "Ahmad Nabili", studentResult.getName());
		Assert.assertEquals("Gagal = GPA tidak sesuai", 3.78, studentResult.getGpa(), 0.0);
	}
	
	@Test
	public void testCreateStudent(){
		StudentModel student = new StudentModel("126", "Budi", 3.44, null);
		// Cek apakah student sudah ada
		Assert.assertNull(service.selectStudent(student.getNpm()));
		// Masukkan ke service
	    service.addStudent(student);
	    // Cek apakah student berhasil dimasukkan
	    Assert.assertNotNull("Mahasiswa gagal dimasukkan", service.selectStudent(student.getNpm()));
	}
	
	@Test
	public void testUpdateStudent(){
		double newGpa = 4;
		StudentModel studentResult = service.selectStudent("1606954691");
		studentResult.setGpa(newGpa);
		service.updateStudent(studentResult);
		// Ambil data student, lalu cek apakah terupdate
		StudentModel updatedStudent = service.selectStudent("1606954691");
		Assert.assertEquals("Gagal = update GPA tidak berhasil", newGpa, updatedStudent.getGpa(), 0.0);
	}
	
	@Test
	public void testDeleteStudent(){
		StudentModel studentResult = service.selectStudent("1606954691");
		// Cek data yang dipilih tidak null
		Assert.assertNotNull("Gagal = studentResult menghasilkan null ", studentResult);
		// Hapus student
		service.deleteStudent(studentResult.getNpm());
		// Ambil data student, lalu cek apakah sudah terhapus
		StudentModel deletedStudent = service.selectStudent("1606954691");
		Assert.assertNull("Gagal = penghapusan student tidak berhasil", deletedStudent);
	}

}
